**Projet HR Management**
**Initialisé le 24/09/2019 en formation Octo**

**Lancer mySql**

docker run  --name hrmanagement \
  --env MYSQL_ROOT_PASSWORD=password \
  --env MYSQL_DATABASE=hrmanagement \
  --publish 3306:3306 \
  mysql:5.7
  
**Se connecter au serveur MySQL créé via docker :**

$ mysql --host=127.0.0.1 --protocol=TCP --user=root --password=password

**Lister les bases de données disponibles**

Une fois connecté au serveur MySQL :

mysql> show databases; 

**Outil de migration:**

Flyway