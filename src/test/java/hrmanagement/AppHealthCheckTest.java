package hrmanagement;

import hrmanagement.controller.HealthCheckerController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@AutoConfigureMockMvc

public class AppHealthCheckTest {
    @Autowired
    private MockMvc mvc;

    @Test
    public void GivenNothing_WhenCheckHealthOfTheApplication_ThenReturnOK() throws Exception {
        //Given
        //When
        //Then
        mvc.perform(MockMvcRequestBuilders.get("/healthcheck"))
                //.andExpect(status().isOk())
                .andExpect(content().json("{" +
                        "\"healthcheckIsOK\":true" +
                        "}"));
    }
}
