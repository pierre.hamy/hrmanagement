package hrmanagement;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import hrmanagement.model.Manager;
import hrmanagement.service.ManagerSpace;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;


import java.nio.charset.Charset;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@AutoConfigureMockMvc

public class ManagerControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private ManagerSpace managerSpace;


    public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    @Test
    public void GivenManagerIsEmpty_WhenPostEmptyManager_ThenHTTPReturnIs400() throws Exception {
        //Given
        //When
        //Then
        mvc.perform(MockMvcRequestBuilders.post("/managers"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void GivenManagerIsEmpty_WhenPostCompleteManager_ThenHTTPReturnIs200() throws Exception {
        //Given

        //When
        Manager manager = new Manager("PHAM","Pierre","HAMY","pierre.hamy@gmail.com","10/10/1976");
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(manager );
        //Then
        mvc.perform(MockMvcRequestBuilders.post("/managers")
                .contentType(APPLICATION_JSON_UTF8)
                .content(requestJson))
                .andExpect(status().isCreated());
    }
    @Test
    public void GivenManagerIsNotCreated_WhenGetManager_ThenHTTPReturnIsNotFound() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/managers/UNKNOWN"))
                .andExpect(status().isNotFound());
    }
    @Test
    public void GivenManagerIsCreated_WhenGetManager_ThenHTTPReturnOK() throws Exception {
        //Given
        Manager manager = new Manager("PHAM","Pierre","HAMY","pierre.hamy@gmail.com","10/10/1976");
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(manager);
        //When
        mvc.perform(MockMvcRequestBuilders.post("/managers")
                .contentType(APPLICATION_JSON_UTF8)
                .content(requestJson));
        //Then
        mvc.perform(MockMvcRequestBuilders.get("/managers/PHAM"))
                .andExpect(status().isOk())
        ;
    }
}
