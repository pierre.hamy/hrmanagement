package hrmanagement;

import hrmanagement.model.Managee;
import hrmanagement.model.Manager;
import hrmanagement.service.ManagerSpace;
import org.junit.Test;

import java.util.List;

import static junit.framework.TestCase.*;

public class ManagerSpaceTest {
    @Test
    public void GivenNoManager_WhenCreateManager_ThenGetNewManager() throws Exception {
        //Given
        Manager manager = new Manager("PHAM","Pierre","HAMY","pierre.hamy@gmail.com","10/10/1976");
        //When
        String managerString = manager.getString();
        //Then
        assertEquals("PHAMPierreHAMYpierre.hamy@gmail.com", managerString);
    }
    @Test
    public void GivenManagerSpaceIsEmpty_WhenRequestedManagerSpace_ThenDisplayEmptyManagerList() throws Exception {
        //Given
        Boolean ManagerSpaceIsEmpty = false;
        Manager manager = new Manager("PHAM","Pierre","HAMY","pierre.hamy@gmail.com","10/10/1976");
        ManagerSpace managerSpace  = new ManagerSpace(manager);
        //When
        ManagerSpaceIsEmpty = managerSpace.isEmpty();
        //Then
        assertTrue(ManagerSpaceIsEmpty);
    }
    @Test
    public void GivenManagerSpaceIsNotEmpty_WhenRequestedManagerSpace_ThenDisplayNotEmptyManagerList() throws Exception {
        //Given
        Boolean managerSpaceIsEmpty = false;
        Manager manager = new Manager("PHAM","Pierre","HAMY","pierre.hamy@gmail.com","10/10/1976");
        ManagerSpace managerSpace  = new ManagerSpace(manager);
        Managee managee = new Managee("TOTO","prénom1","Nom","prenom1.nom1@gmail.com","10/10/1976");
        managerSpace.addManagee(managee);
        //When
        managerSpaceIsEmpty = managerSpace.isEmpty();
        //Then
        assertFalse(managerSpaceIsEmpty);
    }
    @Test
    public void GivenManagerSpaceIsNotEmpty_WhenRequestedManagerSpace_ThenDisplayManagerList() throws Exception {
        //Given
        List<Managee> manageeListAsArray;
        Manager manager = new Manager("PHAM","Pierre","HAMY","pierre.hamy@gmail.com","10/10/1976");
        ManagerSpace managerSpace  = new ManagerSpace(manager);
        Managee managee = new Managee("TOTO","prénom1","Nom","prenom1.nom1@gmail.com","10/10/1976");
        managerSpace.addManagee(managee);
        //When
        manageeListAsArray = managerSpace.getManageeListAsArray();
        String manageeAsString = manageeListAsArray.get(0).getString();

        //Then
        assertEquals("TOTOprénom1Nomprenom1.nom1@gmail.com",manageeAsString);
    }
    @Test
    public void GivenManagerExists_WhenBirthDateIsRequested_ThenDisplayManagerAge() throws Exception {
        //Given
        Manager manager = new Manager("PHAM","Pierre","HAMY","pierre.hamy@gmail.com","10/10/1976");
        ManagerSpace managerSpace  = new ManagerSpace(manager);
        //When
        int managerAge = managerSpace.getManagerAge();
        //Then
        assertEquals(42,managerAge);
    }
}
