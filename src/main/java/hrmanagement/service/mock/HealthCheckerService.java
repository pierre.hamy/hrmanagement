package hrmanagement.service.mock;

import hrmanagement.service.HealthChecker;
import hrmanagement.service.IHealthCheckerService;
import hrmanagement.service.IManagerStore;
import hrmanagement.service.mock.ManagerStore;
import org.springframework.stereotype.Service;

@Service("mockHealthCheck")
public class HealthCheckerService implements IHealthCheckerService {

    private HealthChecker healthChecker;

    public HealthCheckerService(HealthChecker healthChecker) {
        this.healthChecker = healthChecker;
    }

    public HealthChecker getHealthCheck() {

        try {
            IManagerStore managerStore = new ManagerStore();
            System.out.println("ManagerStore créé");
            healthChecker.setHealthcheckIsOK(true);
        }
        catch (Exception e) {
            System.out.println("Erreur connexion au store");

            healthChecker.setHealthcheckIsOK(false);
        }
        finally {
            return healthChecker;
        }
    }
}
