package hrmanagement.service.mock;

import hrmanagement.service.IManagerStore;
import hrmanagement.service.ManagerSpace;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component("mock")
public class ManagerStore implements IManagerStore {

    private Map<String, ManagerSpace> managerSpaceMap;
    private static ManagerStore managerStore = null;

    public static ManagerStore getInstance() {
        if (managerStore == null) {
            managerStore = new ManagerStore();
            return managerStore;
        } else {
            return managerStore;
        }
    }

    public ManagerStore() {
        managerSpaceMap = new HashMap<>();
    }

    public void add(ManagerSpace managerSpace){
        managerSpaceMap.put(managerSpace.getManager().getQuadrigramme(), managerSpace);
    }

    public Map<String,ManagerSpace> getManagerSpaceMap() {
        return managerSpaceMap;
    }


}
