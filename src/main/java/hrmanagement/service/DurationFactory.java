package hrmanagement.service;

import java.time.Period;

public class DurationFactory implements IDurationFactory {

    public int getDurationInYear(java.time.LocalDate firstDate, java.time.LocalDate lastDate) {
        return Period.between(firstDate, lastDate).getYears();
    }
}
