package hrmanagement.service;

import org.springframework.stereotype.Component;

@Component
public class HealthChecker {

    private Boolean healthcheckIsOK = false;

    public void setHealthcheckIsOK(Boolean healthcheckIsOK) {
        this.healthcheckIsOK = healthcheckIsOK;
    }
    public Boolean getHealthcheckIsOK() {
        return healthcheckIsOK;
    }

}
