package hrmanagement.service;

import org.springframework.stereotype.Component;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Map;

@Component
public interface IManagerStore {
    void add(ManagerSpace managerSpace);
    Map<String,ManagerSpace> getManagerSpaceMap() ;

}
