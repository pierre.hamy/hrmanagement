package hrmanagement.service;

public interface IHealthCheckerService {
    HealthChecker getHealthCheck();
}

