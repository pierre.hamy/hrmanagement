package hrmanagement.service;

import hrmanagement.model.Managee;
import hrmanagement.model.Manager;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ManagerSpace {

    private Manager manager;
    private List<Managee> manageeList;

    public ManagerSpace() {
        manageeList = new ArrayList<>();
    }
    public ManagerSpace(Manager manager) {
        this.manager = manager;
    }

    public Manager getManager() {
        return manager;
    }

    public void setManager(Manager manager) {
        this.manager = manager;
    }

    public void setManageeList(List<Managee> manageeList) {
        this.manageeList = manageeList;
    }

    public Boolean isEmpty() {
        if (this.manageeList.size() == 0)
            return true;
        else
            return false;
    }

    public void addManagee(Managee managee) {
        manageeList.add(managee);
    }

    public List<Managee> getManageeListAsArray() {
        return this.manageeList;
    }

    public Integer getManagerAge() {
        return manager.computeAge();
    }
}
