package hrmanagement.service.sql;

import hrmanagement.service.HealthChecker;
import hrmanagement.service.IHealthCheckerService;
import hrmanagement.service.ManagerStoreConfiguration;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Service("sqlHealthCheck")
public class HealthCheckerService implements IHealthCheckerService {

    public final String url;
    public final String username;
    public final String password;

    private HealthChecker healthChecker;

    public HealthCheckerService(HealthChecker healthChecker, ManagerStoreConfiguration sqlManagerStoreConfiguration) {
        this.healthChecker = healthChecker;
        this.url = sqlManagerStoreConfiguration.getUrl();
        this.username = sqlManagerStoreConfiguration.getUsername();
        this.password = sqlManagerStoreConfiguration.getPassword();
    }

    public HealthChecker getHealthCheck() {
        try {
            System.out.println("Avant driver");
//            Class.forName("org.h2.Driver");
            Connection con = DriverManager.getConnection(url, username, password);
            System.out.println("ManagerStore créé");
            healthChecker.setHealthcheckIsOK(true);
        } catch (SQLException e) {
            System.out.println("Erreur connexion au store");
            e.printStackTrace();
            System.out.println(e.getMessage());
//            System.out.println(());
            healthChecker.setHealthcheckIsOK(false);
        }
        finally {
            return healthChecker;
        }

    }
}
