package hrmanagement.service.sql;

import hrmanagement.service.IManagerStore;
import hrmanagement.service.ManagerSpace;
import hrmanagement.service.ManagerStoreConfiguration;
import org.springframework.stereotype.Component;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Map;
@Component("sql")
public class ManagerStore implements IManagerStore {

    private static ManagerStore managerStore = null;
    public final String url;
    public final String username;
    public final String password;


    public ManagerStore(ManagerStoreConfiguration sqlStoreConfiguration) {
        this.url = sqlStoreConfiguration.getUrl();
        this.username = sqlStoreConfiguration.getUsername();
        this.password = sqlStoreConfiguration.getPassword();
    }

    public void add(ManagerSpace managerSpace){
    }

    public Map<String,ManagerSpace> getManagerSpaceMap() {
        return null;
    }

    public Boolean isAlive() {
        try {
            DriverManager.getConnection(url, username, password);
        } catch (SQLException e) {
            return false;
        }

        return true;
    }
}
