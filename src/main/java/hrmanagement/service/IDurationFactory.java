package hrmanagement.service;

public interface IDurationFactory {
    public int getDurationInYear(java.time.LocalDate firstDate, java.time.LocalDate lastDate);
}
