package hrmanagement.model;

import hrmanagement.service.DurationFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class Person {
    private String quadrigramme;
    private String firstName;
    private String lastName;
    private String eMail;
    private Date birthdate;

    public Person() throws ParseException {
        super();
    }
    public Person(String quadrigramme, String firstName, String lastName, String eMail, String birthDate) throws ParseException {
        this.quadrigramme = quadrigramme;
        this.firstName = firstName;
        this.lastName = lastName;
        this.eMail = eMail;
        this.birthdate = new SimpleDateFormat("dd/MM/yyyy").parse(birthDate);

    }


    public String getQuadrigramme() {
        return quadrigramme;
    }

    public void setQuadrigramme(String quadrigramme) {
        this.quadrigramme = quadrigramme;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public String getString() {
        return this.quadrigramme.concat(this.firstName).concat(this.lastName).concat(this.eMail);
    }

    public int computeAge() {
        int age = 0;
        DurationFactory durationFactory = new DurationFactory();
        age = durationFactory.getDurationInYear(LocalDate.from(birthdate.toInstant().atZone(ZoneId.systemDefault())),LocalDate.now());
        return age;
    }

}
