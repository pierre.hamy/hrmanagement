package hrmanagement.model;

import org.springframework.boot.autoconfigure.domain.EntityScan;

import java.text.ParseException;

public class Manager extends Person {

    public Manager() throws ParseException {
        super();
    }
    public Manager(String quadrigramme, String firstName, String lastName, String eMail, String birthDate) throws ParseException {
        super(quadrigramme, firstName, lastName, eMail, birthDate);
    }
}
