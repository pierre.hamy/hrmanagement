package hrmanagement.model;

import java.text.ParseException;

public class Managee extends Person {

    public Managee() throws ParseException {
        super();
    }
    public Managee(String quadrigramme, String firstName, String lastName, String eMail, String birthDate) throws ParseException {
        super(quadrigramme, firstName, lastName, eMail,birthDate);
    }
}
