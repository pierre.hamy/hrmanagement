package hrmanagement;

import hrmanagement.model.Manager;
import hrmanagement.service.ManagerSpace;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.internal.database.base.Connection;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.flyway.FlywayDataSource;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.RestController;

import javax.sql.DataSource;
import java.io.PrintWriter;
import java.sql.DriverManager;

//@SpringBootApplication(scanBasePackages={"hrmanagement.*", "hrmanagement.managercontroller"})
//@ComponentScan({"hrmanagement.*,hrmanagement.managercontroller"})

@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);

        Flyway flyway = new Flyway();
        flyway.setDataSource("jdbc:mysql://localhost:3306", "root", "password");
        flyway.setSchemas("hrmanagement");

        flyway.migrate();
    }
}
