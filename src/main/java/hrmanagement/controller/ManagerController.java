package hrmanagement.controller;

import hrmanagement.service.IManagerStore;
import hrmanagement.service.ManagerSpace;
import hrmanagement.model.Manager;
import hrmanagement.service.mock.ManagerStore;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ManagerController {

    private IManagerStore managerStore;

    public ManagerController(@Qualifier("mock") ManagerStore managerStore) {
        this.managerStore = managerStore;
    }

    @PostMapping("/managers")
    @ResponseStatus(HttpStatus.CREATED)

    public Manager managers(@RequestBody Manager manager) {
        ManagerSpace managerSpace = new ManagerSpace();
        managerSpace.setManager(manager);
        managerStore.add(managerSpace);
        return managerSpace.getManager();
    }

    @GetMapping("/managers/{id}")
    public ResponseEntity<Object> getManager(@PathVariable(value = "id") String quadrigramme) {
        ManagerSpace managerSpace = managerStore.getManagerSpaceMap().get(quadrigramme);
        if (managerSpace == null) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok().body(managerSpace.getManager());
        }

    }
}
