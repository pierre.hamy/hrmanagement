package hrmanagement.controller;


import hrmanagement.service.HealthChecker;
import hrmanagement.service.IHealthCheckerService;
import hrmanagement.service.mock.HealthCheckerService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

@RestController
public class HealthCheckerController {

        private IHealthCheckerService healthCheckerService;
        public HealthCheckerController(@Qualifier("sqlHealthCheck") IHealthCheckerService healthCheckerService) {
            this.healthCheckerService = healthCheckerService;
        }

        @GetMapping("/healthcheck")
        public HealthChecker healthcheck()  {
            return healthCheckerService.getHealthCheck();
        }
    }